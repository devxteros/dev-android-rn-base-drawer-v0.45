import { StackNavigator } from 'react-navigation';
import MainView from './MainView';
import SettingsView from './SettingsView';

export const app = StackNavigator({
  Main: { screen: MainView },
  Settings: { screen: SettingsView },
});